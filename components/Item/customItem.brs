
sub Init()
    m.Poster = m.top.findNode("poster")
end sub

sub itemContentChanged()
    m.Poster.loadDisplayMode = "scaleToZoom"
    if m.top.height < 400 and m.top.width < 400
        m.Poster.loadWidth = 300
        m.Poster.loadHeight = 150
    end if
    updateLayout()
    m.Poster.uri = m.top.itemContent.HDPOSTERURL
end sub

sub updateLayout()
    if m.top.height > 0 And m.top.width > 0 then
        m.Poster.width  = m.top.width
        m.Poster.height = m.top.height
    end if
end sub
