
' Parses the response string as JSON
function parseInitialPlaylistResponse(responseBody as String)
  playlists = []

  if responseBody <> invalid and responseBody <> ""
    jsonData = ParseJSON(responseBody)
    playlistCollectionData = unravel(jsonData, "data.StandardCollection.containers")

    for each rawPlaylist in playlistCollectionData
      rawPlaylistDetails = unravel(rawPlaylist, "set", {})
      playlist = parsePlaylistObject(rawPlaylistDetails)
      playlists.push(playlist)
    end for
  end if

  return playlists
end function

function parsePlaylistDetailsResponse(responseBody as String) as Object
  playlist = invalid

  if responseBody <> invalid and responseBody <> ""
    jsonData = ParseJSON(responseBody)
    rawPlaylistDetails = unravel(jsonData, "data.CuratedSet")

    playlist = parsePlaylistObject(rawPlaylistDetails)
  end if

  return playlist
end function

' Creates a ContentNode for a row of media items
function createRow(list as object)
  row = createObject("RoSGNode", "ContentNode")
  row.Title = list.Title

  for each itemAA in list.ContentList
    item = createObject("RoSGNode","ContentNode")
    AddAndSetFields(item, itemAA)
    row.appendChild(item)
  end for

  return row
end function

' Parses the raw playlist response data from the server into a normalized playlist object
' Returns: playlist as {
'   title: String
'   items: [Object]
'   refId: String
' }
function parsePlaylistObject(rawPlaylistDetails as Object) as Object
  playlistTitle = unravel(rawPlaylistDetails, "text.title.full.set.default.content")
  rawItems = unravel(rawPlaylistDetails, "items")

  playlist = {
    title: playlistTitle
    items: []
    refId: ""
  }

  ' If the data has media items, parse them into objects
  if isNonEmptyArray(rawItems)
    playlistItems = []
    for each rawItem in rawItems
      item = parseMediaObject(rawItem)
      playlistItems.push(item)
    end for

    if isNonEmptyArray(playlistItems)
      playlist.items = playlistItems
    end if
  else
    ' Store the refId so that a detailed playlist can be requested later
    refId = unravel(rawPlaylistDetails, "refId")
    playlist.refId = refId
  end if

  return playlist
end function

' Parses a raw media item into a normalized object
' Returns : item as Object {
'   stream: {url : String},
'   url:  String,
'   streamformat:  String,
'   hdposterurl:  String,
'   hdbackgroundimageurl:  String,
'   uri:  String,
'   releaseDate:  String,
'   description:  String
' }
function parseMediaObject(rawItem as Object) as Object
  mediaType = unravel(rawItem, "type", "")

  tileImages = unravel(rawItem, "image.tile", {})
  aspectImageObj = tileImages["1.78"]

  backgroundImages = unravel(rawItem, "image.background", {})
  backgroundImageObj = backgroundImages["1.78"]

  imageUrl = ""
  backgroundImageUrl = ""
  streamUrl = ""
  if mediaType = "DmcVideo"
    imageUrl = unravel(aspectImageObj, "program.default.url", "")
    backgroundImageUrl = unravel(backgroundImageObj, "program.default.url", "")
    playbackUrls = unravel(rawItem, "mediaMetadata.playbackUrls", [])
    streamUrl = unravel(playbackUrls[0], "href", "")
  else if mediaType = "DmcSeries"
    imageUrl = unravel(aspectImageObj, "series.default.url", "")
    backgroundImageUrl = unravel(backgroundImageObj, "series.default.url", "")
    videoArt = unravel(rawItem, "videoArt", [])
    streamUrls = unravel(videoArt[0], "mediaMetadata.urls", [])
    streamUrl = unravel(streamUrls[0], "url", "")
  else if mediaType = "StandardCollection"
    imageUrl = unravel(aspectImageObj, "default.default.url", "")
    backgroundImageUrl = unravel(backgroundImageObj, "default.default.url", "")
  end if

  releaseInfo = unravel(rawItem, "releases", [])
  releaseDate = unravel(releaseInfo[0], "releaseDate", "")

  item = {
    stream: {url : streamUrl},
    url:  streamUrl,
    streamformat:  "mp4",
    hdposterurl:  imageUrl,
    hdbackgroundimageurl:  backgroundImageUrl,
    uri:  backgroundImageUrl,
    releaseDate:  releaseDate,
    description:  ""
  }

  return item
end function