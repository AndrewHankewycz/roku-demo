' ********** Copyright 2016 Roku Corp.  All Rights Reserved. **********

' init(): UriFetcher constructor
' Description: sets the execution function for the UriFetcher
' 						 and tells the UriFetcher to run
sub init()
  ' create the message port
  m.port = createObject("roMessagePort")

  ' fields for checking if content has been loaded
  m.top.contentSet = false

  ' setting callbacks for url request and response
  m.top.observeField("request", m.port)

  ' setting the task thread function
  m.top.functionName = "go"
  m.top.control = "RUN"
end sub

' go(): The "Task" function.
'   Has an event loop which calls the appropriate functions for
'   handling requests made by the HeroScreen and responses when requests are finished
sub go()
	' UriFetcher event loop
  ' Wait for a request string from the HeroScreen
  while true
    msg = wait(0, m.port)
    mt = type(msg)
    ' If a request was made
    if mt = "roSGNodeEvent"
      if msg.getField()="request"
        if addRequest(msg.getData()) <> true
          m.top.errorMessage = "Invalid request"
        end if
      else
        m.top.errorMessage = "Error: unrecognized field '" + msg.getField() + "'"
      end if
    else
      m.top.errorMessage = "Error: unrecognized event type '" + mt + "'"
    end if
  end while
end sub

' ------------------------
' Task Thread Methods
' ------------------------

' addRequest():
'   Makes the HTTP request
' parameters:
'		request: a String containing the type of request the handler should perform.
' return value:
'   True if request succeeds
' 	False if invalid request
function addRequest(request as Object) as Boolean
  success = true

  if type(request) = "roString"
    ' This is the only type of request the hadler currently knows how to perform.
    if request = "requestHomeScreenContent"
      responseBody = requestInitialPlaylistData()
      if responseBody <> invalid and responseBody <> ""
        content = processInitialPlaylistResponse(responseBody)
        m.top.content = content
      else
        success = false
      end if
    else
      success = false
    end if
  else
    success = false
  end if

  return success
end function

function requestInitialPlaylistData()
  return executeRequest(initialCategoriesEndpoint())
end function

' Executes a HTTP GET request and waits for the response
' Retures the response body as a String
function executeRequest(uri as String) as Object
  if m.urlXfer = invalid
    m.urlXfer = createObject("roUrlTransfer")
    m.urlXfer.setCertificatesFile("common:/certs/ca-bundle.crt")
  end if
  
  m.urlXfer.setUrl(uri)
  return m.urlXfer.GetToString()
end function

' processInitialPlaylistResponse():
'   Processes the HTTP response body.
'   Sets the `m.top.content` field with the response info when complete.
' parameters:
' 	responseBody: the response body as a String
' Returns: A ContentNode constructed with rows of content items.
function processInitialPlaylistResponse(responseBody as Object) as Object
  playlists = parseInitialPlaylistResponse(responseBody)

  ' Iterate over the list of playlist, finding empty ones and requesting full playlist details on them
  for i = 0 to playlists.count() - 1
    playlist = playlists[i]
    ' If the playlist is incomplete, request a detailed playlist using the refId
    if not playlist.items.count() > 0 and playlist.refId <> ""
      playlistUrl = playlistDetailsEndpoint().replace("<refId>", playlist.refId)
      playlistResponse = executeRequest(playlistUrl)
      
      ' Replace the incomplete playlist in the array with the new completed one
      detailedPlaylist = parsePlaylistDetailsResponse(playlistResponse)
      playlists[i] = detailedPlaylist
    end if
  end for

  ' Assemble all the playlists into ContentNodes under a parent node. For the RowList
  parent = createObject("RoSGNode", "ContentNode")

  for each playlist in playlists
    if isNonEmptyArray(playlist.items)
      rowData = {
        Title: playlist.title
        ContentList : playlist.items
      }
      rowNode = createRow(rowData)
      parent.appendChild(rowNode)
    end if
  end for

  return parent
end function

' ------------------------
' Helpers
' ------------------------

function initialCategoriesEndpoint() as String
  return "https://cd-static.bamgrid.com/dp-117731241344/home.json"
end function

function playlistDetailsEndpoint() as String
  return "https://cd-static.bamgrid.com/dp-117731241344/sets/<refId>.json"
end function