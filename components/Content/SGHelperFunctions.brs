' ********** Copyright 2016 Roku Corp.  All Rights Reserved. **********

' Helper function to select only a certain range of content
function select(array as object, first as integer, last as integer) as object
  result = []
  for i = first to last
    result.push(array[i])
  end for
  return result
end function

' Helper function to add and set fields of a content node
function AddAndSetFields(node as object, aa as object)
  addFields = {}
  setFields = {}
  for each field in aa
    if node.hasField(field)
      setFields[field] = aa[field]
    else
      addFields[field] = aa[field]
    end if
  end for
  node.setFields(setFields)
  node.addFields(addFields)
end function

function isNonEmptyArray(array as Object) as Boolean
  return array <> invalid and GetInterface(array, "ifArray") <> invalid and array.Count() > 0
end function

' Somewhat safely extracts values of nested fields within objects
' Obj: The object to extract data from
' path: the path to the object. E.x. fieldA.nestedFieldB.nestedFieldC
' default: the default value to return if the path could not be reached
function unravel(obj as Object, path as String, default = invalid as Dynamic) as Dynamic
  keys = []
  if path <> invalid and path <> ""
    keys = path.split(".")
  end if

  currentObj = obj
  for each key in keys
    if currentObj = invalid
      exit for
    end if

    currentObj = currentObj.LookupCI(key)
  end for

  if currentObj = invalid
    currentObj = default
  end if

  return currentObj
end function
