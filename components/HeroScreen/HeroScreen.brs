' ********** Copyright 2016 Roku Corp.  All Rights Reserved. **********

' Called when the HeroScreen component is initialized
sub Init()
  'Get references to child nodes
  m.RowList       =   m.top.findNode("RowList")
  m.background    =   m.top.findNode("Background")

  'Create a task node to fetch the UI content and populate the screen
  m.UriHandler = CreateObject("roSGNode", "UriHandler")

  ' Set up an observer when the UriHanler has finished requesting data
  ' and another observer to monitor if an error was encountered when making requests.
  m.UriHandler.observeField("content", "onContentChanged")
  m.UriHandler.observeField("errorMessage", "onErrorMessageSet")

  ' Kicks off the request for media content
  requestContent()

  'Create observer events for when content is loaded
  m.top.observeField("visible", "onVisibleChange")
  m.top.observeField("focusedChild", "OnFocusedChildChange")
end sub

' Issues a request for content to the UriHandler component
sub requestContent()
  m.UriHandler.request = "requestHomeScreenContent"
end sub

' observer function to handle when content loads
sub onContentChanged()
  m.top.content = m.UriHandler.content
end sub

' observer function to handle when the UriHandler encounteres an error
sub onErrorMessageSet()
  ' If any error message was set, simply set the error status to true to show a generic message to the user.
  if m.UriHandler.errorMessage <> ""
    m.top.errorStatus = true
  end if
end sub

' handler of focused item in RowList
sub OnItemFocused()
  itemFocused = m.top.itemFocused

  'When an item gains the key focus, set to a 2-element array,
  'where element 0 contains the index of the focused row,
  'and element 1 contains the index of the focused item in that row.
  if itemFocused.Count() = 2 then
    focusedContent            = m.top.content.getChild(itemFocused[0]).getChild(itemFocused[1])
    if focusedContent <> invalid then
      m.top.focusedContent    = focusedContent
      m.background.uri        = focusedContent.hdBackgroundImageUrl
    end if
  end if
end sub

' sets proper focus to RowList in case channel returns from Details Screen
sub onVisibleChange()
  if m.top.visible then m.rowList.setFocus(true)
end sub

' set proper focus to RowList in case if return from Details Screen
Sub onFocusedChildChange()
  if m.top.isInFocusChain() and not m.rowList.hasFocus() then m.rowList.setFocus(true)
End Sub
