' ********** Copyright 2016 Roku Corp.  All Rights Reserved. **********

' 1st function that runs for the scene on channel startup
sub init()
  ' HeroScreen Node with RowList
  m.HeroScreen = m.top.FindNode("HeroScreen")
  ' DetailsScreen Node with description & video player
  m.DetailsScreen = m.top.FindNode("DetailsScreen")
  ' The spinning wheel node
  m.LoadingIndicator = m.top.findNode("LoadingIndicator")
  ' Dialog box node. Appears if content can't be loaded
  m.WarningDialog = m.top.findNode("WarningDialog")
  ' Transitions between screens
  m.FadeIn = m.top.findNode("FadeIn")
  m.FadeOut = m.top.findNode("FadeOut")
  ' Set focus to the scene
  m.top.setFocus(true)
end sub

' Hero Grid Content handler fucntion. If content is set, stops the
' loadingIndicator and focuses on GridScreen.
sub OnChangeContent()
  m.loadingIndicator.control = "stop"
  if m.top.content <> invalid
    m.HeroScreen.visible = true
    m.HeroScreen.setFocus(true)
  else
    showErrorMessage("An unexpected error occured. Press '*' or OK or '<-' to dismiss.")
  end if
end sub

' Error handler fucntion. If the HeroScreen sets it's errorStatus, stops the
' loadingIndicator and presents an error message.
sub OnErrorStatusChanged()
  showErrorMessage("Requests for content failed. Press '*' or OK or '<-' to dismiss.")
end sub

sub showErrorMessage(msg as String)
  m.loadingIndicator.control = "stop"
  m.WarningDialog.visible = true
  m.WarningDialog.message = msg
end sub

' Row item selected handler function.
' On select any item on home scene, show Details node and hide Grid.
sub OnRowItemSelected()
  m.FadeIn.control = "start"
  m.HeroScreen.visible = false
  m.DetailsScreen.content = m.HeroScreen.focusedContent
  m.DetailsScreen.setFocus(true)
  m.DetailsScreen.visible = true
end sub

' Called when a key on the remote is pressed
function onKeyEvent(key as String, press as Boolean) as Boolean
  result = false
  if press then
    if key = "back"
      ' if WarningDialog is open
      if m.WarningDialog.visible = true
        m.WarningDialog.visible = false
        m.HeroScreen.setFocus(true)
        result = true
      ' if Details opened
      else if m.HeroScreen.visible = false and m.DetailsScreen.videoPlayerVisible = false
        m.FadeOut.control = "start"
        m.HeroScreen.visible = true
        m.detailsScreen.visible = false
        m.HeroScreen.setFocus(true)
        result = true
      ' if video player opened
      else if m.HeroScreen.visible = false and m.DetailsScreen.videoPlayerVisible = true
        m.DetailsScreen.videoPlayerVisible = false
        result = true
      end if
    else if key = "OK"
      if m.WarningDialog.visible = true
        m.WarningDialog.visible = false
        m.HeroScreen.setFocus(true)
      end if
    else if key = "options"
      m.WarningDialog.visible = false
      m.HeroScreen.setFocus(true)
    end if
  end if
  return result
end function
